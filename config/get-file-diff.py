import difflib

import requests


def diff_to_html(diff_text):
    diff_lines = diff_text.splitlines()
    diff_html = difflib.HtmlDiff().make_file(
        diff_lines[2:], diff_lines[0], diff_lines[1], context=True
    )
    return diff_html


url = "https://gitea.com/api/v1/repos/DaVinci-L/ws-front/git/commits/4890063e1acbc4ac27c6758992d320fd04b92f57.diff"
headers = {"Authorization": "token 86605d27c3c1bac3da353cda0de3892a6d5aff71"}
response = requests.get(url, headers=headers)

if response.status_code == 200:
    diff_text = response.text

    html_output = diff_to_html(diff_text)
    with open("D:\documents\py_project\gitea-test\diff_output.html", "w") as f:
        f.write(html_output)

    print("HTML文件已生成")
else:
    print(
        "Failed to retrieve repository information. Status code:", response.status_code
    )
