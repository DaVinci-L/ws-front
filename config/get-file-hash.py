import hashlib

import requests

# Gitea API endpoint for getting file contents and SHA value
url = "https://gitea.com/api/v1/repos/DaVinci-L/ws-front/contents/src/App.vue"
headers = {"Authorization": "token 86605d27c3c1bac3da353cda0de3892a6d5aff71"}
response = requests.get(url, headers=headers)


def calculate_sha(content):
    return hashlib.sha1(content.encode("utf-8")).hexdigest()


if response.status_code == 200:
    file_info = response.json()
    sha = file_info["sha"]
    with open("D:\documents\ws-front\src\App.vue", "r") as f:
        new_content = f.read()
    new_sha = calculate_sha(new_content)
    print(f"SHA value of file.txt: {sha}")
    print(f"sha: {new_sha}")
else:
    print("Failed to retrieve file information. Status code:", response.status_code)
