import base64

import requests

url = "https://gitea.com/api/v1/repos/DaVinci-L/ws-front/contents/src/App.vue"
headers = {"Authorization": "token 86605d27c3c1bac3da353cda0de3892a6d5aff71"}
response = requests.get(url, headers=headers)

if response.status_code == 200:
    content = response.json()["content"]
    file_content = base64.b64decode(content).decode("utf-8")
    print(file_content)
else:
    print(
        "Failed to retrieve repository information. Status code:", response.status_code
    )
